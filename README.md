Zdecydowałem się na projekt oparty na sieciach neuronowych.
Głównym założeniem jest stworzenie AI, które bazując na zdobytych danych, będzie potrafiło samo "grać w gry".
Planuję skorzystać z pythonowskiej biblioteki TenserFlow i opierać się na architekturze konwolucyjnych sieci neuronowych,
bądź wstecznej propagacji. Wstępny pomysł opiera się na uczeniu się AI od użytkownika zasad gier, by potem go doskonale imitować.